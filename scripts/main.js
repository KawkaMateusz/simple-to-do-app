// submit add taks
const submit = document.querySelector('#submit-add-task');
submit.addEventListener('click', addTask);

// generate existing tasks
const body = document.querySelector('body');
body.addEventListener('onload', generateWorks())


function addTask() {
    event.preventDefault();
    // take task name from field
    let taskName = document.querySelector('#taskInput').value;

    // remove white space for generate task id
    let taskId = taskName.replace(/ /g, '');
    taskId += 'id';

    // task object
    let task = {
        id: taskId,
        name: taskName
    }
    
    // validation task name
    if (validation(taskName) === false) {
        return console.log('fail - task must have more than 3 characters & can\'t existing');
    };
    
    // check existing local storage 
    if (localStorage.getItem('works') === null) {

        //init array
        works = [];

        // add tasks to array
        works.push(task);

        //set to local storage
        localStorage.setItem('works', JSON.stringify(works))
    } else {
        //get works from local storage
        let works = JSON.parse(localStorage.getItem('works'));

        //add task to array
        works.push(task);

        //set to local storage
        localStorage.setItem('works', JSON.stringify(works))
    }

    
    // generate exisitng tasks
    generateWorks();
}

function generateWorks() {
    // check existing tasks
    if (localStorage.getItem('works') === null) {

        //init array
        let works = [];
        
        //set to local storage
        localStorage.setItem('works', JSON.stringify(works))
    }
    // get tasks from local storage
    let works = JSON.parse(localStorage.getItem('works'));

    let result = document.querySelector('#todo-list');
    result.innerHTML = '';

    // generate html tasks
    for (let i = 0; i < works.length; i++) {
        result.innerHTML += '<div class="task" id=\'' + works[i].id + '\'<div>' +
            '<h6>' + works[i].name + '</div>' +
            '<a onclick="deleteTask(\'' + works[i].id + '\')" class="btn btn-danger btn-sm" href="#">Delete</a>' + '<a onclick="doneTask(\'' + works[i].id + '\')" class="btn btn-success btn-sm" href="#">Done</a>' +
            '</h3>' +
            '</div>';
    }
}

function deleteTask(id) {
    // get tasks list from local storage
    let works = JSON.parse(localStorage.getItem('works'));

    // search task in tasks array
    for (let i = 0; i < works.length; i++) {
        if (id === works[i].id) {
            works.splice(i, 1);
            console.log('task has been deleted')
        }

        // update tasks in local storage
        localStorage.setItem('works', JSON.stringify(works));
    }

    // generate exisitng tasks
    generateWorks();
}

function doneTask(id) {
    // get tasks list from local storage
    let works = JSON.parse(localStorage.getItem('works'));
    for (let i = 0; i < works.length; i++) {

        //searching task in tasks array
        if (id === works[i].id) {
            let task = document.getElementById(id);
            task.className += ' done-task'
        }

        // update tasks in local storage
        localStorage.setItem('works', JSON.stringify(works));
    }

    // generate exisitng tasks
    generateWorks();
}

function validation(taskName) {
    let valid = false;

    // check if taskName length is more than 3
    if (taskName.length > 2) {
        valid = true;;
    }

    // get tasks list from local storage
    let works = JSON.parse(localStorage.getItem('works'));

    // check if task name existing in tasks array
    for (let i = 0; i < works.length; i++) {
        if (taskName === works[i].name) {
            valid = false;
        }
    }
    return valid;
}
